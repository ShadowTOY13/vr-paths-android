using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StopSceneButton : MonoBehaviour
{
    private bool isClick = false;

    public void OnPointerEnter()
    {
        isClick = true;
        Invoke("CloseActiveScene", 3.0f);
    }

    public void OnPointerExit()
    {
        isClick = false;
    }

    private void CloseActiveScene()
    {
        if (isClick)
        {
            SceneManager.LoadScene("MenuScene", LoadSceneMode.Single);
        }
    }
}
