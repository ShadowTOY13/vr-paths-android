using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedirectMenu : MonoBehaviour
{
    public GameObject from;
    public GameObject to;
    private bool isClick = false;

    /// <summary>
    /// This method is called by the Main Camera when it starts gazing at this GameObject.
    /// </summary>
    public void OnPointerEnter()
    {
        isClick = true;
        Invoke("OpenMenu", 2.0f);
    }

    /// <summary>
    /// This method is called by the Main Camera when it stops gazing at this GameObject.
    /// </summary>
    public void OnPointerExit()
    {
        isClick = false;
    }

    private void OpenMenu()
    {
        if (isClick)
        {
            from.SetActive(false);
            to.SetActive(true);
        }
    }
}
