using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpenScene : MonoBehaviour
{
    public string sceneName;
    private bool isClick = false;

    /// <summary>
    /// This method is called by the Main Camera when it starts gazing at this GameObject.
    /// </summary>
    public void OnPointerEnter()
    {
        isClick = true;
        Invoke("Open", 2.0f);
    }

    /// <summary>
    /// This method is called by the Main Camera when it stops gazing at this GameObject.
    /// </summary>
    public void OnPointerExit()
    {
        isClick = false;
    }

    private void Open()
    {
        if (isClick)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        }
    }
}
