using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quit : MonoBehaviour
{
    private bool isClick = false;

    /// <summary>
    /// This method is called by the Main Camera when it starts gazing at this GameObject.
    /// </summary>
    public void OnPointerEnter()
    {
        isClick = true;
        Invoke("QuitApp", 2.0f);
    }

    /// <summary>
    /// This method is called by the Main Camera when it stops gazing at this GameObject.
    /// </summary>
    public void OnPointerExit()
    {
        isClick = false;
    }

    private void QuitApp()
    {
        if (isClick)
        {
            Debug.Log("Quit");
            Application.Quit();
        }
    }
}
